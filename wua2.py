import win32com
import win32com.client
import win32api
#Searching, Downloading, and Installing Updates
#https://msdn.microsoft.com/en-us/library/windows/desktop/aa387102(v=vs.85).aspx
#https://msdn.microsoft.com/en-us/library/windows/desktop/aa386526(v=vs.85).aspx

global updateSearcher
global searchResult

def PrcessUpdateCategories(collection):
    collectionCategories = []

    try:
        for collectionIndex in range(collection.Count):
            update = collection.Item(collectionIndex)
            for categoryIndex in range(update.Categories.Count):
                name = update.Categories.Item(categoryIndex).Name
                if name not in collectionCategories:
                    print('\t[-]Update Category: {0} {1}'.format(categoryIndex,name))
                    collectionCategories.append(name)
    except:
        print(':ERROR -  Update Categories')
    return collectionCategories

def GetUpdateSearcher(updateSession):
    updateSearcher = None
    try:
        updateSearcher = updateSession.CreateUpdateSearcher()
    except:
        print(':ERROR - Getting Searcher')
    return updateSearcher

def GetAvailableUpdates(updateSearcher):
    searchResult = None
    try:
        print('Searching for Updates...')
        #searchResult = updateSearcher.Search('IsInstalled=0 or IsHidden=0 or Type=\'Software\'')
        searchResult = updateSearcher.Search('IsInstalled=0 and IsHidden=0 and Type=\'Software\' or Type=\'Driver\'')
        print('[+]Updates {0} found'.format(str(searchResult.Updates.Count)))
        print('....')
    except:
        print(':ERROR-Searching')
    return searchResult
    
def UpdateSearcher(searchResult,collection):
 
    try:
        if (searchResult.Updates.Count == 0):
            print('No applicable updates')
            
        for update in searchResult.Updates:
            print('[+]{0}'.format(str(update)))
            if update.InstallationBehavior.CanRequestUserInput:
                print('[+]Skipped update: Request for User Input Required for:  {0}'.format(str(update)))
                continue
            if update.IsDownloaded:
                print('[+]Update is Downloaded {0} '.format(str(update)))
                continue
  
            for category in update.Categories:
                print('[+]Category: {0}'.format(str(category.Name)))
                #if category.Name in categories:
                if update.EulaAccepted == False:
                    print('[+]Skipping Eula Required update: ', update.EulaText)
                    print('\t[-]'.format(str(update)))
                else:
                    collection.Add(update)
                    print('\t[-]Add: {0}'.format(str(update)))
                break
        categoryName = PrcessUpdateCategories(collection)
        print('>>>Categories: {0} <<<'.format(str(categoryName)))
    except:
        print(':ERROR - parsing updates failed.')
        
    return collection 
        
def GetUpdateSession():
    print('[+]Getting the session object.')
    update_session = win32com.client.Dispatch('Microsoft.Update.Session')
    return update_session

def GetUpdateCollection():
    #print('[+]applicable updates.')
    collection = win32com.client.Dispatch('Microsoft.Update.UpdateColl')
    return collection

def GetUpdateDownloader(session):
    
    print('[+] Geting Download object:')
    downloader = session.CreateUpdateDownloader()
    print(downloader)     
    return downloader

def ProcessDownloader(updateSession,updatesToDownloadList):
    downloader = None
    try:
        print('[+]Start Downloading ...')
        downloader = GetUpdateDownloader(updateSession)
        downloader.Updates = updatesToDownloadList
        try:
            resultDownload = downloader.Download()
            print('[+]Download Result: ',resultDownload.ResultCode)
            for i in range(updatesToDownloadList.Count):
                print('\t[-] {0} {1}'.format(str(updatesToDownloadList.Item(i).Title), str(resultDownload.GetUpdateResult(i).ResultCode)))
            
        except Exception as e:
            print('downloader.Download() ISSUE {0}'.format(str(e)))
        print('Perform DOWNLOAD OK')
        
    except:
        print(':ERROR - download error')
    return downloader

    
def IsUpdateEnabled(serviceManager):
    serviceList = serviceManager.Services
    #print('Service List Count:', serviceList.Count)
    for service in serviceList:
        if service.name == 'Microsoft Update':
            return True
    return false

def ProcessUpdateInstaller(session,updatesToInstall):
    installer = session.CreateUpdateInstaller()
    installer.Updates = updatesToInstall
    installationResult = installer.Install()
    print('[+] Installation Result: ',installationResult.ResultCode)
    for installUpdate in updatesToInstall:
        print('[+]Update Installed: {0) {1}'.format(str(installUpdate.Title),installationResult.GetUpdateResult(installUpdate).ResultCode))

def GetServiceManager():
    updateServiceManager = win32com.client.Dispatch('Microsoft.Update.ServiceManager')
    return updateServiceManager


def PrintAvailableUpdates(searchResults):
    listUpdates = searchResults.Updates
    if (listUpdates.Count == 0):
        print('No updates found')
    else:
        for update in searchResults.Updates:
            print('Title: {0}'.format(str(update.Title)))

def PrintMicrosoftUpdateEnabled(serviceManager):
    serviceList = serviceManager.Services
    print('[+] Services registered with WUA: ', serviceList.Count)
    index = 0
    for service in serviceList:
        index = index + 1
        print('\t[-] {0}: {1}'.format(index,str(service.name)))

    print("-")
    microsoftUpdateEnabled = IsUpdateEnabled(serviceManager)
    if (microsoftUpdateEnabled):
        print('[+] Microsoft Update: ENABLED')
    else:
        print('[+] Microsoft Update: NOT ENABLED')

              
              
def ReportUpdatesToDownload(updatesToDownloadList):
    try:          
        print('[+]Updates to download',updatesToDownloadList.Count);
    except:
        print(':ERROR - reporting updates to download count')
              
def Main():
    updateToDownload = GetUpdateCollection()
    updateSession = GetUpdateSession()
    updateSearcher = GetUpdateSearcher(updateSession)
    searchResults = GetAvailableUpdates(updateSearcher)
    serviceManager = GetServiceManager()
    #Prints if Microsoft update is Enabled
    PrintMicrosoftUpdateEnabled(serviceManager)

    #Prints if Microsoft update is available
    PrintAvailableUpdates(searchResults)
    
    updatesToDownloadList = UpdateSearcher(searchResults,updateToDownload)
    ReportUpdatesToDownload(updatesToDownloadList)
    
    #updateToDownload2 = GetUpdateCollection()
    if updatesToDownloadList.Count > 0:
        downloader = ProcessDownloader(updateSession,updatesToDownloadList)
    else:
        print('******NOTHING to Download')
    
    #ProcessUpdateInstaller(updateSession,updatesToDownloadList)
    print('Thanks...')
        
if(__name__ == "__main__"):
    Main();
